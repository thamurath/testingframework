//
// Created by adminuser on 3/16/15.
//


#include "FileLoader.h"
#include "Exceptions.hpp"

#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/stream.hpp>
#include <cstdint>

const uint64_t FileLoader::null_position = std::string::npos;
const FileLoader::FilePosition FileLoader::NullFilePosition = std::make_pair(FileLoader::null_position, FileLoader::null_position);

const uint64_t FileLoader::s_initialColumn = 1;
const uint64_t FileLoader::s_initialLine = 1;

const FileLoader::FilePosition FileLoader::s_initialPosition = std::make_pair(FileLoader::s_initialLine, FileLoader::s_initialColumn);

FileLoader::FileLoader(const std::string& ai_completeFileName)
{
  try
  {
    boost::iostreams::stream<boost::iostreams::mapped_file_source> mappedFile(ai_completeFileName);
    mappedFile.unsetf(std::ios::skipws); // avoid using whitespaces as delimiters

    for(std::string line; std::getline(mappedFile, line, '\n'); )
    {
      m_lines.push_back(line);
    }
  }
  catch (std::ios::failure e)
  {
    throw NoSuchFileOrDirectoryException(ai_completeFileName);
  }
}

FileLoader::~FileLoader(void)
{
  m_lines.clear();
}

std::uint64_t FileLoader::GetNumLines(void) const
{
  return m_lines.size();
}

std::string FileLoader::GetLine(const uint64_t& ai_numLine) const
{
  if ( (s_initialLine > ai_numLine) || (ai_numLine > m_lines.size()) )
  {
    throw NumberOfLinesOutOfBoundsException(ai_numLine,s_initialLine,m_lines.size());
  }
  return GetLineInternal(ai_numLine);
}

inline std::string FileLoader::GetLineInternal(uint64_t const& ai_numLine) const
{
  return m_lines[ai_numLine-1];
}

uint64_t FileLoader::FindInLine(const std::string ai_expression, FilePosition const& ai_initialPosition) const
{
  if ( 0 == ai_expression.length() )
  {
    throw InvalidExpressionException();
  }

  if (false == CheckPosition(ai_initialPosition))
  {
    throw FilePositionOutOfBoundsException(ai_initialPosition,s_initialPosition,std::make_pair(m_lines.size(),m_lines[m_lines.size()-1].length()));
  }
  return FindInLineInternal(ai_expression, ai_initialPosition);
}

inline uint64_t FileLoader::FindInLineInternal(std::string const& ai_expresion, FileLoader::FilePosition const& ai_initialPosition) const
{
  std::string line(GetLineInternal(ai_initialPosition.first));
  uint64_t column (line.find(ai_expresion, ai_initialPosition.second-1));
  return (std::string::npos == column)?(null_position):(column + 1);
}

FileLoader::FilePosition FileLoader::FindFirst(const std::string& ai_expresion, FilePosition const& ai_initialPosition) const
{
  if ( 0 == ai_expresion.length() )
  {
    throw InvalidExpressionException();
  }

  if (false == CheckPosition(ai_initialPosition))
  {
    throw FilePositionOutOfBoundsException(ai_initialPosition,s_initialPosition,std::make_pair(m_lines.size(),m_lines[m_lines.size()-1].length()));
  }

  const uint64_t max_lines (GetNumLines());

  uint64_t line (ai_initialPosition.first);
  uint64_t  column(ai_initialPosition.second);
  uint64_t position(std::string::npos);
  do
  {
    position = FindInLineInternal(ai_expresion, std::make_pair(line,column));

    if (std::string::npos != position)
    {
      return std::make_pair(line,position);
    }

    ++line;
    column = s_initialColumn;
  } while ((max_lines > line));


  return NullFilePosition;
}

inline bool FileLoader::CheckPosition(const FilePosition& ai_position) const
{
  return !
      (
        ( (ai_position.first < s_initialLine) || ( ai_position.first > GetNumLines()) ) ||
        ( ( ai_position.second < s_initialColumn) || (ai_position.second > m_lines[ai_position.first-1].length()) )
      );
}

uint64_t FileLoader::CountInLine(const std::string& ai_expresion, const uint64_t& ai_numLine) const
{
  if (0 == ai_expresion.length())
  {
    throw InvalidExpressionException();
  }
  if ( (s_initialLine > ai_numLine) || (ai_numLine > m_lines.size()) )
  {
    throw NumberOfLinesOutOfBoundsException(ai_numLine,s_initialLine,m_lines.size());
  }
  return CountInLineInternal(ai_expresion, ai_numLine);

}

inline uint64_t FileLoader::CountInLineInternal(std::string const& ai_expresion, uint64_t const& ai_numLine) const
{
  std::string line ( GetLineInternal(ai_numLine));
  if ( line.length() < ai_expresion.length())
  {
    return 0;
  }

  uint64_t count(0);

  for (
        std::string::size_type offset(line.find(ai_expresion));
        std::string::npos != offset;
        offset = line.find(ai_expresion, offset + ai_expresion.length())
      )
  {
    ++count;
  }

  return count;
}

uint64_t FileLoader::Count(const std::string& ai_expresion) const
{
  if (0 == ai_expresion.length())
  {
    throw InvalidExpressionException();
  }

  const uint64_t max_lines(GetNumLines());
  uint64_t retValue(0);
  for( uint64_t idx(s_initialLine); idx < max_lines; ++idx)
  {
    retValue += CountInLineInternal(ai_expresion,idx);
  }
  return retValue;
}

FileLoader::FilePosition FileLoader::FindNext(const std::string& ai_expresion, FilePosition const& ai_initialPosition) const
{
  if (false == CheckPosition(ai_initialPosition))
  {
    throw FilePositionOutOfBoundsException(ai_initialPosition,s_initialPosition,std::make_pair(m_lines.size(),m_lines[m_lines.size()-1].length()));
  }

  const uint64_t max_lines (GetNumLines());

  uint64_t line (ai_initialPosition.first);
  uint64_t column(ai_initialPosition.second + ai_expresion.length());
  uint64_t position(std::string::npos);
  do
  {
    position = FindInLineInternal(ai_expresion, std::make_pair(line,column));

    if (std::string::npos != position)
    {
      return std::make_pair(line,position);
    }

    ++line;
    column = s_initialColumn;
  } while ((max_lines > line));


  return NullFilePosition;
}
