//
// Created by adminuser on 3/16/15.
//

#ifndef _TESTINGFRAMEWORK_FILELOADER_H_
#define _TESTINGFRAMEWORK_FILELOADER_H_

#include <boost/noncopyable.hpp>

#include <string>
#include <cstdint>
#include <vector>
#include <cstdint>

class sealed
{
protected:
  sealed() {}
  virtual ~sealed() {}
};


class FileLoader : private boost::noncopyable, private sealed
{
public:
  /**
  * Return value when there is nothing interesting to return
  */
  static const uint64_t null_position;

  /**
  * File position:
  * first -> line
  * second -> column
  */
  typedef std::pair<uint64_t, uint64_t > FilePosition;

  /**
  * File position for not-found.
  */
  static const FilePosition NullFilePosition;

  /**@brief FileLoader constructor.
  *
  * Loads ai_completeFileName **TEXT FILE** into memory.
  *
  * @param[in] ai_completeFileName : Full Qualified name of the file to be loaded.
  *
  * @throw NoSuchFileOrDirectoryException
  */
  FileLoader(const std::string& ai_completeFileName);
  ~FileLoader(void);


  /**@brief Number of lines of text file loaded into memory.
  *
  * @return Number of lines.
  */
  std::uint64_t GetNumLines(void) const;


  /**@brief Return a line.
  *
  * @param[in] ai_numLine Line number desired ( 1 based)
  *
  * @return a string with the desired line.
  *
  * @throw NumberOfLinesOutOfBoundsException if ai_numline is greater than the number of lines.
  */
  std::string GetLine(const uint64_t& ai_numLine) const;

  /**@brief Find an expression in a text line starting from initial position.
  *
  * @param[in] ai_expression : string to look for
  * @param[in] ai_initialPosition : starting position.
  *
  * @return Column where the expression was found, null_position if nothing found.
  *
  * @throw InvalidExpressionException if expression in empty
  *
  * @note This is a first version so regular expression are **NOT** supported yet
  */
  uint64_t FindInLine(const std::string ai_expression, FilePosition const& ai_initialPosition) const;

  /**@brief Look for first occurrence of an expression in the file.
  *
  * @param[in] ai_expression : string to look for
  * @param[in] ai_initialPosition : starting position.
  *
  * @return FilePosition(line,column) where the expression was found ( both line and column are 1 based numbers) or NullPosition if nothing found.
  *
  * @throw InvalidExpressionException if expression in empty
  *
  * @note This is a first version so regular expression are **NOT** supported yet
  */
  FileLoader::FilePosition FindFirst(const std::string& ai_expresion, FilePosition const& ai_initialPosition = std::make_pair(1, 1)) const;

  /**@brief Count the number of times an expression appears in a line.
  *
  * @param[in] ai_expression : string to look for
  * @param[in] ai_numLine : Line to search.
  *
  * @return column where the expression was found ( column is a 1 based number) or null_position if nothing found.
  *
  * @throw InvalidExpressionException if expression in empty
  *
  * @note This is a first version so regular expression are **NOT** supported yet
  */
  uint64_t CountInLine(const std::string& ai_expresion, const uint64_t& ai_numLine) const;

  /**@brief Count the number of times an expression appears in the file.
  *
  * @param[in] ai_expression : string to look for
  *
  * @return FilePosition<line,column> where the expression was found ( both line and column are 1 based numbers) or NullPosition if nothing found.
  *
  * @throw InvalidExpressionException if expression in empty
  *
  * @note This is a first version so regular expression are **NOT** supported yet
  */
  uint64_t Count(const std::string& ai_expresion) const ;


  /**@brief Look for the next occurrence of an expression in the file.
  *
  * @param[in] ai_expression : string to look for
  * @param[in] ai_initialPosition : starting position, usually a value returned by FindFirst
  *
  * It will look for ai_expression starting at the line indicated by ai_initialPosition
  * and the column calculated as ai_initialPosition.column + ai_expression.length
  *
  * @return FilePosition(line,column) where the expression was found ( both line and column are 1 based numbers) or NullPosition if nothing found.
  *
  * @throw InvalidExpressionException if expression in empty
  *
  * @note This is a first version so regular expression are **NOT** supported yet
  */
  FilePosition FindNext(const std::string& ai_expresion, FilePosition const& ai_initialPosition) const;
protected:
private:
  std::vector<std::string> m_lines;

  uint64_t FindInLineInternal(std::string const& ai_expresion, FilePosition const& ai_initialPosition) const;

  uint64_t CountInLineInternal(std::string const& ai_expresion, uint64_t const& ai_numLine) const;

  std::string GetLineInternal(uint64_t const& ai_numLine) const;

  static const uint64_t s_initialColumn;
  static const uint64_t s_initialLine;
  static const FilePosition s_initialPosition;

  bool CheckPosition(const FilePosition& ai_position) const;

};


#endif //_TESTINGFRAMEWORK_FILELOADER_H_
