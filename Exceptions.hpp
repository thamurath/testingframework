//
// Created by adminuser on 3/17/15.
//

#ifndef _TESTINGFRAMEWORK_EXCEPTIONS_HPP_
#define _TESTINGFRAMEWORK_EXCEPTIONS_HPP_

#include <cstdint>
#include <exception>
#include <sstream>
#include <string>
#include <cstring>

class NumberOfLinesOutOfBoundsException : virtual public std::exception
{
public:
  NumberOfLinesOutOfBoundsException(const uint64_t& ai_incoming
                        , const uint64_t& ai_minimun
                       , const uint64_t& ai_maximun)
      : m_incoming(ai_incoming)
      , m_minimun(ai_minimun)
      , m_maximun(ai_maximun)
  {
    //nothing else to do
  }
  virtual ~NumberOfLinesOutOfBoundsException(void) throw()
  {
    //nothing to do
  }

  virtual const char* what(void) const throw()
  {
    std::stringstream ss;
    ss << "Incoming parameter: " << m_incoming
        << ", out of valid bounds for lines numbers: min[" << m_minimun <<"]"
        << " max["<< m_maximun <<"]";
    return ss.str().c_str();
  }

private:
  uint64_t m_incoming;
  uint64_t m_minimun;
  uint64_t m_maximun;
};

class FilePositionOutOfBoundsException : virtual public std::exception
{
public:
  FilePositionOutOfBoundsException(const FileLoader::FilePosition& ai_incoming
      , const FileLoader::FilePosition& ai_minimun
      , const FileLoader::FilePosition& ai_maximun)
      : m_incomingLine(ai_incoming.first)
      , m_incomingColumn(ai_incoming.second)
        , m_minimunLine(ai_minimun.first)
        , m_minimunColumn(ai_minimun.second)
        , m_maximunLine(ai_maximun.first)
        , m_maximunColumn(ai_maximun.second)

  {
    //nothing else to do
  }
  virtual ~FilePositionOutOfBoundsException(void) throw()
  {
    //nothing to do
  }

  virtual const char* what(void) const throw()
  {
    std::stringstream ss;
    ss << "Incoming parameter: <line,col>:<" << m_incomingLine << "," << m_incomingColumn << ">"
        << ", out of valid bounds for file positions: min<line,col>: <" << m_minimunLine <<"," << m_minimunColumn<<">"
        << " max<line,col>: <" << m_maximunLine <<"," << m_maximunColumn<<">";
    return ss.str().c_str();
  }

private:
  uint64_t m_incomingLine;
  uint64_t m_incomingColumn;
  uint64_t m_minimunLine;
  uint64_t m_minimunColumn;
  uint64_t m_maximunLine;
  uint64_t m_maximunColumn;
};

class NoSuchFileOrDirectoryException : virtual public std::exception
{
public:
  NoSuchFileOrDirectoryException(const std::string& ai_completeFileName)
      : m_completeFileName(NULL)
  {
    if ( 0 < ai_completeFileName.length() )
    {
      m_completeFileName = new char[ai_completeFileName.length()+1];
      std::memcpy(m_completeFileName,ai_completeFileName.c_str(),ai_completeFileName.length());
      m_completeFileName[ai_completeFileName.length()] = '\0';
    }
  }
  virtual ~NoSuchFileOrDirectoryException(void) throw()
  {
    //nothing to do
    delete[] m_completeFileName;
    m_completeFileName = NULL;
  }

  virtual const char* what(void) const throw()
  {
    std::stringstream ss;
    ss << "Unable to open File: " << m_completeFileName;
    return ss.str().c_str();
  }

private:
  char* m_completeFileName;

};


class InvalidExpressionException : virtual public std::exception
{
public:
  InvalidExpressionException(const std::string& ai_expression = "")
      : m_expression(NULL)
  {
    if ( 0 < ai_expression.length() )
    {
      m_expression = new char[ai_expression.length()+1];
      std::memcpy(m_expression,ai_expression.c_str(),ai_expression.length());
      m_expression[ai_expression.length()] = '\0';
    }
  }
  virtual ~InvalidExpressionException(void) throw()
  {
    //nothing to do
    delete[] m_expression;
    m_expression = NULL;
  }

  virtual const char* what(void) const throw()
  {
    if (NULL != m_expression)
    {
      std::stringstream ss;
      ss << "Invalid incoming expression: " << m_expression;
      return ss.str().c_str();
    }
    return "Invalid incoming expression";
  }

private:
  char* m_expression;

};


#endif //_TESTINGFRAMEWORK_EXCEPTIONS_HPP_


