



//#define CATCH_CONFIG_MAIN
//#include "catch.hpp"


#include "FileLoader.h"

#include <iostream>

int main(void)
{
  FileLoader quijote("./textfile.txt");

  uint64_t num_lines(quijote.GetNumLines());
  std::cout << "Numero de lineas: " << num_lines << std::endl;

//  std::cout << quijote.CheckPosition(std::make_pair(0,0)) << std::endl; // ambos mal
//  std::cout << quijote.CheckPosition(std::make_pair(1,100)) << std::endl; // linea bien col mal
//  std::cout << quijote.CheckPosition(std::make_pair(1,10)) << std::endl; // ambos bien
//
//  std::cout << quijote.CheckPosition(std::make_pair(num_lines,0)) << std::endl; // col mal
//  std::cout << quijote.CheckPosition(std::make_pair(num_lines,1)) << std::endl; // bien
//  std::cout << quijote.CheckPosition(std::make_pair(num_lines,10)) << std::endl; // col mal
//  std::cout << quijote.CheckPosition(std::make_pair(num_lines+1,1)) << std::endl; // bien



//  for (uint64_t idx(0); idx < num_lines; ++idx )
//  {
//    std::cout << quijote.GetLine(idx) << std::endl;
//  }

  std::cout << quijote.FindInLine("Cervantes",std::make_pair(31,1)) << std::endl;

  FileLoader::FilePosition pos (quijote.FindFirst("Cervantes", std::make_pair(9, 1)));
  std::cout << "Cervantes found in line:" << pos.first << " column:" << pos.second << std::endl;

//  pos = quijote.FindFirst("Cervantes");
//  std::cout << "Cervantes found in line:" << pos.first << " column:" << pos.second << std::endl;
//  pos = quijote.FindNext("Cervantes",pos);
//  std::cout << "Cervantes found in line:" << pos.first << " column:" << pos.second << std::endl;
//  pos = quijote.FindNext("Cervantes",pos);
//  std::cout << "Cervantes found in line:" << pos.first << " column:" << pos.second << std::endl;

  std::cout << quijote.Count("Cervantes") << std::endl;

  const uint64_t k_count(quijote.Count("Cervantes"));

  for(FileLoader::FilePosition idx(quijote.FindFirst("Cervantes"));FileLoader::NullFilePosition != idx; idx = quijote.FindNext("Cervantes",idx) )
  {
    std::cout << "Line:" << idx.first << " Col:" <<idx.second << std::endl;
  }

  std::cout << quijote.CountInLine("e",41) << std::endl;

  std::cout << quijote.GetLine(1) << std::endl;
  std::cout << quijote.GetLine(num_lines) << std::endl;




  //std::cout << quijote.FindFirst("Cervantes") << std::endl;
}

