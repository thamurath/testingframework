#define CATCH_CONFIG_MAIN

#include "catch.hpp"



#include "FileLoader.h"
#include "Exceptions.hpp"

#include <string>

// you can set as many tags as you want for a test case ... then you can select which tags you want to run..
TEST_CASE("Failing always showing log macros" ,"[FileLoader] [basic] [fail] [log]")
{
  std::string file_name ("./textfile.txt");

  // Capture variables to be shown when test fails
  CAPTURE( file_name );


  // you may add as many info messages as you want ... those messages will not be shown on the screen
  // ( you can use the -s flag to see this messages )
  INFO("Creating file...");

  // A warn is always shown ...
  WARN("File should throw an exception " << file_name);

  // can
  //REQUIRE_THROWS_AS( FileLoader f(file_name) , NoSuchFileOrDirectoryException);

  FAIL("fuck off!");
}

TEST_CASE("Counting strings","[FileLoader] [count] [assertion]")
{
  std::string filename("./textfile.txt");
  const std::string Title("El ingenioso hidalgo don Quijote de la Mancha");
  try
  {
    FileLoader textFile(filename);

    // Check will continue the test and will fail at the end
    CHECK (14 == textFile.Count("Cervantes") );
    CHECK_THROWS(textFile.GetLine(0));
    CHECK_THROWS_AS(textFile.GetLine(0), NumberOfLinesOutOfBoundsException);

    // Require will terminate the test if fails ...
    REQUIRE( Title == textFile.GetLine(1) );

    REQUIRE_FALSE(Title == textFile.GetLine(2));



  }
  catch (NoSuchFileOrDirectoryException e)
  {
    FAIL("Error: " << e.what());
  }


}


TEST_CASE("Check exceptions" ," [exceptions] [FileLoader]")
{

  const std::string filename("./textfile.txt");
  const std::string Title("El ingenioso hidalgo don Quijote de la Mancha");

  FileLoader file(filename);

  //sometimes you need the same common code for multiple related test ... so SECTIONS are useful here ...

  SECTION("Check Counting methods do not throw any exception even on error")
  {
    //some of this checks will fail ... but all of them will be tested ...
    CHECK_NOTHROW(file.Count("xxXX"));
    CHECK_NOTHROW(file.Count(""));
    CHECK_NOTHROW(file.CountInLine("xxx", 3));
    CHECK_NOTHROW(file.CountInLine("ingeniose",1));
    CHECK_NOTHROW(file.CountInLine("ingeniose",0));
  }
  SECTION("Check Counting methods throw exceptions even on error")
  {
    //some of this checks will fail ... but all of them will be tested ...
    REQUIRE_NOTHROW(file.Count("xxXX"));
    REQUIRE_THROWS_AS(file.Count(""), InvalidExpressionException);
    CHECK_NOTHROW(file.CountInLine("xxx", 3));
    REQUIRE_NOTHROW(file.CountInLine("ingenioso",1));
    REQUIRE_THROWS_AS( file.CountInLine("ingeniose",0), NumberOfLinesOutOfBoundsException);
  }
}

//you can use even BDD to make it easier ...
SCENARIO("Text strings can be searched", "[find][FileLoader]")
{
  GIVEN(" A text file loaded into a FileLoader ...")
  {
    const std::string filename ("./textfile.txt");
    FileLoader file(filename);

    REQUIRE(file.GetNumLines() > 0);

    WHEN( "trying to search using a regular expression")
    {
      THEN("you do not find anything because regular expressions are not supported yet")
      {
        REQUIRE(0 == file.Count("Cerva?tes"));
      }
    }

    WHEN("searching for a case insensitive string")
    {
      THEN("you will have troubles because searches are case sensitive by default")
      {
        REQUIRE(0 == file.Count("cervantes"));
      }
    }

    WHEN("searching for a case sensitive string")
    {
      THEN("you will find it")
      {
        REQUIRE(0 < file.Count("Cervantes"));
        CHECK(14 == file.Count("Cervantes"));
      }
    }

  }
}