
* FileLoader.h
* FileLoader.cpp
* Exceptions.hpp

These files implements a read-on-memory text file with some methods to test. (boost iostreams library required)
Use these files with your favourite unit test framework


* textfile.txt ==> This a sample text file


* catch.hpp  ==> Is the Catch unit test framework ( nothing else required )

* catch_test.cpp ==> These are my test cases ...


Use the following line to compile the test ...



g++ -std=c++0x -o catch_test FileLoader.cpp catch_test.cpp -lboost_iostreams